﻿using BeepTest.Models;
using BeepTest.Repositories;
using BeepTest.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BeepTest.Controllers.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private IPlayerRepository _playerRepository;

        public TestController(IPlayerRepository player)
        {
            _playerRepository = player;
        }
        // GET: api/<TestController>
        [HttpGet("GetPlayers")]
        public ActionResult GetPlayers()
        {
            return Ok(_playerRepository.GetPlayers());
        }

        [HttpGet("FitnessRating")]
        public ActionResult GetFitnessRating()
        {
            var fitnessRatingData = new List<FitnessRating>();
            var folderDetails = Path.Combine(Directory.GetCurrentDirectory(), $"wwwroot\\{"json\\fitnessrating_beeptest.json"}");
            var jsonText = System.IO.File.ReadAllText(folderDetails);

            JArray jsonArray = JArray.Parse(jsonText);
            foreach (var item in jsonArray)
            {
                var jsonObj = JsonConvert.DeserializeObject<FitnessRating>(item.ToString());
                fitnessRatingData.Add(jsonObj);
            }

            return Ok(fitnessRatingData);
        }

        // GET api/<TestController>/5
        [HttpGet("WarnPlayer/{id}")]
        public ActionResult WarnPlayer(int id)
        {
            var allPlayersData = _playerRepository.GetPlayers();
            try
            {
                int editIndex = allPlayersData.FindIndex(o => o.id == id);
                allPlayersData[editIndex].warn = true;
                return Ok(allPlayersData[editIndex]);
            }
            catch (Exception)
            {
                return NotFound();
            }

        }

        // POST api/<TestController>
        [HttpPost("ResultPlayer/{id}")]
        public ActionResult ResultPlayer([FromForm] PlayerResultViewModel playerResultRecieved)
        {
            var playerResult = _playerRepository.resultPlayer(playerResultRecieved.id, playerResultRecieved.result);
            Console.WriteLine(playerResultRecieved.id + " : " + playerResultRecieved.result);

            return Ok(playerResult);
        }
    }
}
