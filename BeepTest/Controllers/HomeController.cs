﻿using BeepTest.Models;
using BeepTest.Repositories;
using BeepTest.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace BeepTest.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private IPlayerRepository _playerRepository;

        public HomeController(ILogger<HomeController> logger,IPlayerRepository player)
        {
            _logger = logger;
            _playerRepository = player;
        }

        public IActionResult Index()
        {
            var homeViewModel = new HomeViewModel();

            var allPlayers = _playerRepository.GetPlayers();

            homeViewModel.players = allPlayers;
            return View(homeViewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
