﻿using BeepTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeepTest.ViewModel
{
    public class HomeViewModel
    {
        public IEnumerable<Player> players { get; set; }
    }
}
